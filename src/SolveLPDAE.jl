module SolveLPDAE ### start module

### ---------------------------------------------- ###
### Import libraries                               ###
### ---------------------------------------------- ###
using PyPlot

export
### ---------------------------------------------- ###
### Export types                                   ###
### ---------------------------------------------- ###
RungeKuttaMethod,
MOLDAE,

### ---------------------------------------------- ###
### Export functions                               ###
### ---------------------------------------------- ###
setupRungeKuttaMethod, # functions from "solve_pdae_problem.jl"
setupMOLDAE,
solveMOLDAE,
getSolution,
plotSolution,
calcExactSolution,
calcError,
estConvergenceOrder,
getMethodFilePath,     # functions from "search_file_path.jl"
getExampleFilePath

### ---------------------------------------------- ###
### Include main files                             ###
### ---------------------------------------------- ###
include("solve_pdae_problem.jl");
include("search_file_path.jl")

### ---------------------------------------------- ###
### Include available solution methods.            ###
### ---------------------------------------------- ###
### SAFERK methods:
include("methods/SAFERK.jl")
### RadauIIA methods:
include("methods/RadauIIA.jl")

end ### end module
