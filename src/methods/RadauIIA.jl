### -------------------------------------------------- ###
### Method: RadauIIA(moldae, method, U)                ###
### -------------------------------------------------- ###
### This method is used to advance the solution of the ###
### MOL-DAE in time. More specifically, if this method ###
### is choosen, then it will be possible to use any of ###
### the Radau-IIA methods defined by the files:        ###
### RadauIIA(1).jl, RadauIIA(3).jl,                    ###
### RadauIIA(5).jl                                     ###
### -------------------------------------------------- ###
function RadauIIA{Q<:Real, R<:AbstractFloat, T<:Integer}(moldae::MOLDAE{Q, R, T}, method::RungeKuttaMethod{Q}, U::Array{Float64, 2})
  s = size(method.RKmat, 2);
  I_Nn = speye(moldae.n*moldae.spaceSteps, moldae.n*moldae.spaceSteps);
  I_s = speye(s, s);
  e_s = ones(s, 1);
  K = kron(spzeros(s, 1), spzeros(size(U, 1), size(U, 2)));
  for j = 1:moldae.timeSteps
    A = (kron(I_s, moldae.M)+moldae.tau*kron(method.RKmat, I_Nn)*kron(I_s, moldae.D));
    b = (vectorF(moldae, j, method.c)-kron(I_s, moldae.D)*kron(e_s, U[:, j]));
    K[:, j+1] = A\b;
    U[:, j+1] = U[:, j]+moldae.tau*kron(method.b, I_Nn)*K[:, j+1];
  end
  return(U);
end
