### -------------------------------------------------- ###
### Method: SAFERK(moldae, method, U)                  ###
### -------------------------------------------------- ###
### This method is used to advance the solution of the ###
### MOL-DAE in time. More specifically, if this method ###
### is choosen, then it will be possible to use any of ###
### the SAFERK methods defined by the files:           ###
### SAFERK(0.75, 3).jl, SAFERK(0.73, 4).jl,            ###
### SAFERK(0.74, 4).jl or SAFERK(0.75, 4).jl           ###
### -------------------------------------------------- ###
function SAFERK{Q<:Real, R<:AbstractFloat, T<:Integer}(moldae::MOLDAE{Q, R, T}, method::RungeKuttaMethod{Q}, U::Array{Float64, 2})
  s = size(method.RKmat, 2);
  I_Nn = speye(moldae.n*moldae.spaceSteps, moldae.n*moldae.spaceSteps);
  I_s = speye(s-1, s-1);
  e_s = ones(s-1, 1);
  S = kron(spzeros(s-1, 1), spzeros(size(U, 1), size(U, 2)));
  for j = 1:moldae.timeSteps
    A = (kron(I_s, moldae.M)+moldae.tau*kron(method.RKmat[2:s, 2:s], moldae.D));
    b = ((kron(e_s, moldae.M)-moldae.tau*kron(method.RKmat[2:s, 1:1], moldae.D))*U[:,j]+moldae.tau*kron(method.RKmat[2:s, 1:s], I_Nn)*vectorF(moldae, j, method.c));
    S[:, j+1] = A\b;
    U[:, j+1] = kron(hcat(spzeros(1, s-2), 1), I_Nn)*S[:, j+1];
  end
  return(U);
end
