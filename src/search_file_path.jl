### -------------------------------------------------- ###
### Method: getMethodFilePath(key)                     ###
### -------------------------------------------------- ###
### The method searches the library directory with the ###
### method coefficient files and returns the system    ###
### path for the desired method file. The currently    ###
### availble method files are:                         ###
### For SAFERK methods:                                ###
### SAFERK(0.75, 3).jl                                  ###
### SAFERK(0.73, 4).jl                                 ###
### SAFERK(0.74, 4).jl                                 ###
### SAFERK(0.75, 4).jl                                 ###
###                                                    ###
### For Radau IIA methods:                             ###
### RadauIIA(1).jl                               ###
### RadauIIA(3).jl                               ###
### RadauIIA(5).jl                               ###
### -------------------------------------------------- ###
function getMethodFilePath(key::ASCIIString)
  libPath = replace(@__FILE__, "search_file_path.jl", "");
  file = filter(x->contains(x, key), readdir(libPath * "method_coefficients_files"));
  return(libPath * "method_coefficients_files\\" * file[1]);
end

### -------------------------------------------------- ###
### Method: getExampleFilePath(key)                    ###
### -------------------------------------------------- ###
### The method searches the library directory with the ###
### PDAE problem examples and returns the system path  ###
### for the desired example to solve. The currently    ###
### availble example files are:                        ###
### Example1.jl                                        ###
### Example2.jl                                        ###
### Example3.jl                                        ###
### -------------------------------------------------- ###
function getExampleFilePath(key::ASCIIString)
  libPath = replace(@__FILE__, "search_file_path.jl", "");
  file = filter(x->contains(x, key), readdir(libPath * "pdae_problems"));
  return(libPath * "pdae_problems\\" * file[1]);
end
