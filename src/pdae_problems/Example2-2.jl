#-----------------------------------------------------
#  Define matrices, parameters and functions:
#  Example →→  2.2 (11) ←←
#-----------------------------------------------------
const r = 0.;
const delta = 0.5;
const xS = -1.;
const xE = 1.;
const tS = 0.;
const tE = 1.;
const timeSteps = 25;
const spaceSteps = 25;
A = [0. 2. 0.; 1. -1. 0.; 1. -1. 0.];
B = [-1. 0. 0.; 0. 0. 0.; 0. 0. -1.];
C = [0. 0. 0.; 0. -1. 0.; 0. 0. 1.];
u = [u1(t, x) = (x^2-1)*exp(-t)+t^3*cos(x)^2;
    u2(t, x) = x^2*exp(-(1/2)*t);
    u3(t, x) = (x^2-1)*sin(t)+t^3*sin(x)^2];
f = [f1(t, x) = 4*t^3*cos(x)^2-x^2*exp(-(1/2)*t)-2*t^3-2*exp(-t)  ;
    f2(t, x) = 3*t^2*cos(x)^2-(1/2)*x^2*exp(-(1/2)*t)-exp(-t)*x^2+exp(-t);
    f3(t, x) = -5*t^3*cos(x)^2+3*t^3+3*t^2*cos(x)^2+sin(t)*x^2+(1/2)*x^2*exp(-(1/2)*t)-exp(-t)*x^2-3*sin(t)+exp(-t)];
