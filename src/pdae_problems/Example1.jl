#-----------------------------------------------------
#  Define matrices, parameters and functions:
#  Example →→  1 (10) ←←
#-----------------------------------------------------
const r = 0.;
const delta = 0.5;
const xS = -0.5;
const xE = 0.5;
const tS = 0.;
const tE = 1.;
const timeSteps = 25;
const spaceSteps = 25;
A = [0. 1. 0.; 0. 0. 1.; 0. 0. 0.];
B = [0. 0. -1.; 0. -1. -1.; 0. 0. 0.];
C = [-1. -1. -1.; 0. -1. 0.; 0. 0. -1.];
u = [u1(t, x) = x*(x-1)*sin(t);
    u2(t, x) = x*(x-1)*cos(t);
    u3(t, x) = x*(x-1)*(exp(t)+t^5)];
f = [f1(t, x) = -x*(x-1)*(2*sin(t)+cos(t)+exp(t)+t^5)-2*(exp(t)+t^5);
    f2(t, x) = -2*(cos(t)+exp(t)+t^5)-x*(x-1)*(cos(t)-exp(t)-5*t^4);
    f3(t, x) = -x*(x-1)*(exp(t)+t^5)];
