#-----------------------------------------------------
#  Define matrices, parameters and functions:
#  Example →→  3 (12) ←←
#-----------------------------------------------------
const r = 0.;
const delta = 0.5;
const xS = 0.;
const xE = 1.;
const tS = 0.;
const tE = 1.;
const timeSteps = 25;
const spaceSteps = 25;
const constC = 254.;
const constD = 0.1652;
const constE = 1.;
const constL = 1.32;
A = [0. 0. 0. 0.; 0. 0. (-constL*constC/(xE^2)) (constL/constD); 1. 0. 0. 0.; 0. 1. 0. 0.];
B = [-1. 0. 0. 0.; 0. -1. 0. 0.; 0. 0. 0. 0.; 0. 0. 0. 0.];
C = [0. 1. 0. 0.; 0. 0. 0. 0.; 0. 0. -1. 0.; 0. 0. 0. -1.];
u = [u1(t, x) = (constE/xE-constC*constD*constE*xE/6)*x+constC*constD*constE/(6*xE)*x^3;
   u2(t, x) = constC*constD*constE/xE*x;
   u3(t, x) = 0.;
   u4(t, x) = 0.];
f = [f1(t, x) = 0.;
   f2(t, x) = 0.;
   f3(t, x) = 0.;
   f4(t, x) = 0.];
