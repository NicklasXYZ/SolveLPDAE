#-----------------------------------------------------
#  Define matrices, parameters and functions:
#  Example →→  2.1 (11) ←←
#-----------------------------------------------------
const r = 0.;
const delta = 0.5;
const xS = -1.;
const xE = 1.;
const tS = 0.;
const tE = 1.;
const timeSteps = 25;
const spaceSteps = 25;
A = [0. 2. 0.; 1. -1. 0.; 1. -1. 0.];
B = [-1. 0. 0.; 0. 0. 0.; 0. 0. -1.];
C = [0. 0. 0.; 0. -1. 0.; 0. 0. 1.];
u = [u1(t, x) = x^2*exp(-t);
    u2(t, x) = x^2*exp(-1/2*t);
    u3(t, x) = x^2*sin(t)];
f = [f1(t, x) = -exp(-t/2)*x^2-2*exp(-t);
    f2(t, x) = -exp(-t)*x^2+(exp(-t/2)*x^2)/2-x^2*exp(-t/2);
    f3(t, x) = -exp(-t)*x^2+(exp(-t/2)*x^2)/2-2*sin(t)+sin(t)*x^2];
