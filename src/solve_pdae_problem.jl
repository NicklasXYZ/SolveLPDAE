### ################################################## ###
### Author     : Nicklas Sindlev Andersen.             ###
### Last edited: 13-07-2016.                           ###
### ################################################## ###
### ################################################## ###
### ################################################## ###
### Define types                                       ###
### ################################################## ###

type RungeKuttaMethod{Q<:Real}
  solutionMethod::Function;
  RKmat::Array{Q, 2};
  b::Array{Q, 2};
  c::Array{Q, 1};
end

type MOLDAE{Q<:Real, R<:AbstractFloat, T<:Integer}
  B::Array{Q, 2};
  M::SparseMatrixCSC{Q, Int64};
  D::SparseMatrixCSC{Q, Int64};
  r::R;
  xS::R;
  xE::R;
  tS::R;
  tE::R;
  tau::R;
  h::R;
  n::T;
  timeSteps::T;
  spaceSteps::T;
  discreteTime::Array{R, 1};
  discreteSpace::Array{R, 1};
  u::Array{Function, 1};
  f::Array{Function, 1};
end

### ################################################## ###
### Define methods                                     ###
### ################################################## ###
##  ################################################## ###
### -------------------------------------------------- ###
### Method: setupRungeKuttaMethod(solutionMethod,      ###
###                               RKmat, b, c)         ###
### -------------------------------------------------- ###
### The method constructs and returns a field of type  ###
### RungeKuttaMethod, such that the different compo-   ###
### nents defining the Runge-Kutta method can be ac-   ###
### cessed and used later on.                          ###
### -------------------------------------------------- ###
function setupRungeKuttaMethod{Q<:Real}(solutionMethod::Function, RKmat::Array{Q, 2}, b::Array{Q, 2}, c::Array{Q, 1})
  method = RungeKuttaMethod(solutionMethod, RKmat, b, c);
  return(method);
end

### -------------------------------------------------- ###
### Method: setupMOLDAE(A, B, C, r, delta,             ###
###                    xS, xE,  tS, tE,                ###
###                    timeSteps, spaceSteps,          ###
###                    u, f)                           ###
### -------------------------------------------------- ###
### The method constructs and returns a field of type  ###
### MOLDAE, from the given input.                      ###
### The following quantities are also calculated by    ###
### the method, for the definition of the type:        ###
### tau : Constant time step size.                     ###
### h : Constant grid spacing.                         ###
### n : The size of the system (nr. of equations).     ###
### M : Matrix.                                        ###
### D : Matrix.                                        ###
### discreteTime : Time discretization points.         ###
### discreteSpace : Spatial discretization points.     ###
### -------------------------------------------------- ###
function setupMOLDAE{Q<:Real, R<:AbstractFloat, T<:Integer}(A::Array{Q, 2}, B::Array{Q, 2}, C::Array{Q, 2}, r::R,
  delta::R, xS::R, xE::R, tS::R, tE::R, timeSteps::T, spaceSteps::T, u::Array{Function, 1}, f::Array{Function, 1})
  const tau = (tE-tS)/timeSteps;
  const h = (xE-xS)/(spaceSteps+1);
  const n = size(A, 1);
  I_N = speye(spaceSteps, spaceSteps);
  M = kron(I_N, A);
  d = vec((-2+h*r*(1-2*delta))*ones(spaceSteps, 1));
  du = vec((1+h*r*delta)*ones(spaceSteps-1, 1));
  dl = vec((1+h*r*(delta-1))*ones(spaceSteps-1, 1));
  P = sparse(diagm(d, 0)+diagm(dl, -1)+diagm(du, 1));
  D = 1/(h^2)*kron(P, B)+kron(I_N, C);
  discreteTime = tS+collect(0:1:timeSteps)*tau;
  discreteSpace = xS+collect(1:1:spaceSteps)*h;
  moldae = MOLDAE(B, M, D, r, xS, xE, tS, tE, tau, h, n, timeSteps, spaceSteps, discreteTime, discreteSpace, u, f);
  return(moldae);
end

### -------------------------------------------------- ###
### Method: solveMOLDAE(moldae, method)                ###
### -------------------------------------------------- ###
### This method serves as a wrapper function, for the  ###
### methods advanceMOLDAE and attachBV. The method     ###
### simply fetches the approximate solution (to the    ###
### internal grid points) of the MOL-DAE and attaches  ###
### the appropriate boundary values. The complete so-  ###
### lution 'U' is then returned.                       ###
### -------------------------------------------------- ###
function solveMOLDAE{Q<:Real, R<:AbstractFloat, T<:Integer}(moldae::MOLDAE{Q, R, T}, method::RungeKuttaMethod{Q})
  U = advanceMOLDAE(moldae, method);
  U = attachBV(moldae, U);
  return(U);
end

### ---------------------------------------------------###
### Method: setBV(moldae)                              ###
### -------------------------------------------------- ###
### The following method calculates and returns        ###
### Dirichlet boundary values (at xS and xE) on the    ###
### boundary (time) discretization points.             ###
### -------------------------------------------------- ###
function setBV{Q<:Real, R<:AbstractFloat, T<:Integer}(moldae::MOLDAE{Q, R, T})
  lside = kron(moldae.discreteTime, ones(moldae.n, 1));
  rside = kron(moldae.discreteTime, ones(moldae.n, 1));
  for i = 1:moldae.timeSteps+1
    for j = moldae.n-1:-1:0
      lside[moldae.n*i-j] = moldae.u[moldae.n-j](lside[moldae.n*i-j], moldae.xS);
      rside[moldae.n*i-j] = moldae.u[moldae.n-j](rside[moldae.n*i-j], moldae.xE);
    end
  end
  return(lside, rside);
end

### -------------------------------------------------- ###
### Method: setIV(moldae)                              ###
### -------------------------------------------------- ###
### The method calculates the initial values (at time  ###
### 'tS') on the (space) discretization points.        ###
### Additionally the vector 'U' containing the initial ###
### values, are concatenated with a zero matrix, such  ###
### that the subsequent columns in the matrix can be   ###
### overwritten when advancing the solution with a     ###
### Runge-Kutta method. The newly concatenated matrix  ###
### with the initial values are then returned.         ###
### -------------------------------------------------- ###
function setIV{Q<:Real, R<:AbstractFloat, T<:Integer}(moldae::MOLDAE{Q, R, T})
  U = kron(moldae.discreteSpace, ones(moldae.n, 1));
  for i = 1:moldae.spaceSteps
    for j = moldae.n-1:-1:0
      U[moldae.n*i-j] = moldae.u[moldae.n-j](moldae.tS, U[moldae.n*i-j]);
    end
  end
  U = hcat(U, spzeros(size(U, 1), moldae.timeSteps));
  return(U);
end

### -------------------------------------------------- ###
### Method: getTimeValue(moldae, tStep, k, c )         ###
### -------------------------------------------------- ###
### This method calculates the current time value      ###
### 'tVal', given the previous time value and the 'c'  ###
### value defined by the Runge-Kutta method, at the    ###
### current stage 'k'.                                 ###
### -------------------------------------------------- ###
function getTimeValue{Q<:Real, R<:AbstractFloat, T<:Integer}(moldae::MOLDAE{Q, R, T}, tStep::Int64, k::Int64, c::Array{Q, 1})
  tValprevious = moldae.tS + (tStep-1)*moldae.tau;
  tValcurrent = tValprevious + c[k]*moldae.tau;
  return(tValcurrent);
end

### -------------------------------------------------- ###
### Method: getBV(moldae, tVal, xVal)                  ###
### -------------------------------------------------- ###
### This method determines which values, that should   ###
### enter into the space discretization. This method   ###
### determines the values elementwise by checking the  ###
### matrix B. This should be equivalent to determining ###
### the values Bu(t, x).                               ###
### -------------------------------------------------- ###
function getBV{Q<:Real, R<:AbstractFloat, T<:Integer}(moldae::MOLDAE{Q, R, T}, tVal::Float64, xVal::Float64)
  tmpArray1 = Array(Any, 0);
  tmpArray2 = Array(Float64, 0);
  for i = 1:size(moldae.B, 1)
    for j = 1:size(moldae.B, 2)
      if moldae.B[i,j] != 0
        push!(tmpArray1, [i, j, moldae.B[i, j]]);
      end
    end
  end
  for j = 1:moldae.n
    holdVal = 0;
    for i = 1:size(tmpArray1, 1)
      if tmpArray1[i][1] == j
        holdVal += Int64(tmpArray1[i][3])*moldae.u[Int64(tmpArray1[i][2])](tVal, xVal);
      end
    end
    push!(tmpArray2, holdVal);
  end
  return(tmpArray2);
end

### -------------------------------------------------- ###
### Method: vectorF(moldae, tStep, c)                  ###
### -------------------------------------------------- ###
### This method constructs the vector F that arises    ###
### from the RHS function 'f' (of the PDAE) and the    ###
### boundary values which enter into the discretiza-   ###
### tion. For each time step 'tStep' a vector 'holdF'  ###
### of values are returned.                            ###
### -------------------------------------------------- ###
function vectorF{Q<:Real, R<:AbstractFloat, T<:Integer}(moldae::MOLDAE{Q, R, T}, tStep::Int64, c::Array{Q, 1})
  holdF = Array(Float64, 0);
  for k = 1:size(c, 1)
    tVal = getTimeValue(moldae, tStep, k, c);
    F = kron(moldae.discreteSpace, ones(moldae.n, 1));
    for i = 1:moldae.spaceSteps
      if i == 1 || i == moldae.spaceSteps
        if i == 1
          if moldae.r != 0. && moldae.delta == 0.5
            for j = moldae.n-1:-1:0
              F[moldae.n*i-j] = moldae.f[moldae.n-j](tVal, F[moldae.n*i-j])-1/(moldae.h^2)*
              (getBV(moldae, tVal, moldae.xS)[moldae.n-j]-moldae.h*moldae.r*getBV(moldae, tVal, moldae.xS)[moldae.n-j]/2);
            end
          elseif moldae.r != 0. && moldae.delta == 0.
            for j = moldae.n-1:-1:0
              F[moldae.n*i-j] = moldae.f[moldae.n-j](tVal, F[moldae.n*i-j])-1/(moldae.h^2)*
              (getBV(moldae, tVal, moldae.xS)[moldae.n-j]-moldae.h*moldae.r*getBV(moldae, tVal, moldae.xS)[moldae.n-j]);
            end
          else
            for j = moldae.n-1:-1:0
              F[moldae.n*i-j] = moldae.f[moldae.n-j](tVal, F[moldae.n*i-j])-1/(moldae.h^2)*getBV(moldae, tVal, moldae.xS)[moldae.n-j];
            end
          end
        else
          if moldae.r != 0. && moldae.delta == 0.5
            for j = moldae.n-1:-1:0
              F[moldae.n*i-j] = moldae.f[moldae.n-j](tVal, F[moldae.n*i-j])-1/(moldae.h^2)*
              (getBV(moldae, tVal, moldae.xE)[moldae.n-j]+moldae.h*moldae.r*getBV(moldae, tVal, moldae.xE)[moldae.n-j]/2);
            end
          elseif moldae.r != 0. && moldae.delta == 1.
            for j = moldae.n-1:-1:0
              F[moldae.n*i-j] = moldae.f[moldae.n-j](tVal, F[moldae.n*i-j])-1/(moldae.h^2)*
              (getBV(moldae, tVal, moldae.xE)[moldae.n-j]+moldae.h*moldae.r*getBV(moldae, tVal, moldae.xE)[moldae.n-j]);
            end
          else
            for j = moldae.n-1:-1:0
              F[moldae.n*i-j] = moldae.f[moldae.n-j](tVal, F[moldae.n*i-j])-1/(moldae.h^2)*getBV(moldae, tVal, moldae.xE)[moldae.n-j];
            end
          end
        end
      else
        for j = moldae.n-1:-1:0
          F[moldae.n*i-j] = moldae.f[moldae.n-j](tVal, F[moldae.n*i-j]);
        end
      end
    end
    holdF = vcat(holdF, F);
  end
  return(holdF);
end

### -------------------------------------------------- ###
### Method: advanceMOLDAE(moldae, method)              ###
### -------------------------------------------------- ###
### Another wrapper function that calls the setIV      ###
### method and a specified Runge-Kutta method.         ###
### The method setIV fetches the initial values, such  ###
### that they can be passed to the Runge-Kutta solu-   ###
### tion method, that advances the solution in time.   ###
### -------------------------------------------------- ###
function advanceMOLDAE{Q<:Real, R<:AbstractFloat, T<:Integer}(moldae::MOLDAE{Q, R, T}, method::RungeKuttaMethod{Q})
  U = setIV(moldae);
  U = method.solutionMethod(moldae, method, U);
  return(U);
end

### -------------------------------------------------- ###
### Method: attachBV(moldae, U)                        ###
### -------------------------------------------------- ###
### This method calls the setBV method, which computes ###
### the boundary values. The boundary values are then  ###
### attached to the approximate solution 'U' of the    ###
### internal grid points, such that the complete so-   ###
### lution can be passed on to another method.         ###
### -------------------------------------------------- ###
function attachBV{Q<:Real, R<:AbstractFloat, T<:Integer}(moldae::MOLDAE{Q, R, T}, U::Array{Float64, 2})
  BV = setBV(moldae);
  tmpU = kron(zeros(moldae.spaceSteps+2, 1), zeros(moldae.n, 1));
  tmpU = hcat(tmpU, zeros(size(tmpU, 1), moldae.timeSteps));
  setSize = moldae.n-1;
  for j = 1:moldae.timeSteps+1
    tmpU[:, j] = vcat(BV[1][moldae.n*j-setSize:moldae.n*j], U[:, j], BV[2][moldae.n*j-setSize:moldae.n*j]);
  end
  return(tmpU);
end

### -------------------------------------------------- ###
### Method: findIndex(sol)                             ###
### -------------------------------------------------- ###
### This method simply calculates and returns the      ###
### correct index, given 'solIndex'.                   ###
### -------------------------------------------------- ###
function findIndex{Q<:Real, R<:AbstractFloat, T<:Integer}(moldae::MOLDAE{Q, R, T}, solIndex::Int64)
  for i = moldae.n:-1:0
    tmpVal = i*-1+moldae.n;
    if tmpVal == solIndex
      return(i);
    end
  end
end

### -------------------------------------------------- ###
### Method: getSolution(moldae, U, solIndex)           ###
### -------------------------------------------------- ###
### This method assembles and returns a matrix         ###
### 'solution' with a particular solution of the       ###
### system, given a solution index 'solIndex'.         ###
### -------------------------------------------------- ###
function getSolution{Q<:Real, R<:AbstractFloat, T<:Integer}(moldae::MOLDAE{Q, R, T}, U::Array{Float64, 2}, solIndex::Int64)
  solIndex = findIndex(moldae, solIndex);
  solution = zeros(moldae.spaceSteps+2, 1);
  solution = hcat(solution, zeros(size(solution, 1), moldae.timeSteps));
  for i = 1:moldae.spaceSteps+2
    for j = 1:moldae.timeSteps+1
      solution[i, j] = U[moldae.n*i-solIndex, j];
    end
  end
  return(solution);
end

### -------------------------------------------------- ###
### Method: plotSolution(moldae, solution, solIndex)   ###
### -------------------------------------------------- ###
### This method uses the package 'PyPlot' and plots a  ###
### particular solution given an appropriate matrix    ###
### 'solution'.                                        ###
### -------------------------------------------------- ###
function plotSolution{Q<:Real, R<:AbstractFloat, T<:Integer}(moldae::MOLDAE{Q, R, T}, solution::Array{Float64, 2}, solIndex::Int64)
  x = moldae.xS + collect(0:1:moldae.spaceSteps+1)*moldae.h;
  t = moldae.tS + collect(0:1:moldae.timeSteps)*moldae.tau;
  surf(x, t, transpose(solution), rstride = 1, cstride = 1,
  edgecolors = "k", cmap = ColorMap("winter"), alpha = 0.75, linewidth = 0.00); # linewidth = moldae.tau);
  mesh(x, t, transpose(solution), color = "black", alpha = 0.25);
  title("Solution \$u_{$solIndex}\$", fontsize=14);
  xlabel("Space \$x\$", fontsize=11);
  ylabel("Time \$t\$", fontsize=11);
end

### -------------------------------------------------- ###
### Method: calcExactSolution(moldae)                  ###
### -------------------------------------------------- ###
### This method calculates and returns the exact solu- ###
### tion to the MOL-DAE at the discretization points.  ###
### -------------------------------------------------- ###
function calcExactSolution{Q<:Real, R<:AbstractFloat, T<:Integer}(moldae::MOLDAE{Q, R, T})
  U = kron(moldae.xS + collect(0:1:moldae.spaceSteps+1)*moldae.h, ones(moldae.n,1));
  U = kron(ones(1, moldae.timeSteps+1), U);
  for i = 1:moldae.spaceSteps+2
    for k = 1:moldae.timeSteps+1
      for j = moldae.n-1:-1:0
        U[moldae.n*i-j, k] = moldae.u[moldae.n-j](moldae.discreteTime[k], U[moldae.n*i-j, k]);
      end
    end
  end
  return(U);
end

### -------------------------------------------------- ###
### Method: calcError(moldae)                          ###
### -------------------------------------------------- ###
### This method simply calculates and returns the      ###
### error between the exact and approximate solution.  ###
### -------------------------------------------------- ###
function calcError{R<:AbstractFloat}(approxU::Array{R, 2}, exactU::Array{R, 2})
  holdError = exactU-approxU;
  return(holdError);
end

### -------------------------------------------------- ###
### Method: estConvergenceOrder(moldae1, moldae2,      ###
###                             approxU1, approxU2,    ###
###                             exactU1, exactU2)      ###
### -------------------------------------------------- ###
### This method is used to estimate the convergence    ###
### order in the discrete L2-norm.                     ###
### Given the appropriate input the estimated conver-  ###
### gence order is returned.                           ###
### -------------------------------------------------- ###
function estConvergenceOrder{Q<:Real, R<:AbstractFloat, T<:Integer}(moldae1::MOLDAE{Q, R, T}, moldae2::MOLDAE{Q, R, T},
  approxU1::Array{R, 2}, approxU2::Array{R, 2}, exactU1::Array{R, 2}, exactU2::Array{R, 2})
  holdError1 = calcError(approxU1, exactU1)[1:(moldae1.spaceSteps+2)*moldae1.n, moldae1.timeSteps+1];
  holdError2 = calcError(approxU2, exactU2)[1:(moldae2.spaceSteps+2)*moldae2.n, moldae2.timeSteps+1];
  holdVal1 = sqrt(moldae1.h*sum((holdError1).^2.0));
  holdVal2 = sqrt(moldae2.h*sum((holdError2).^2.0));
  estOrder = log2(holdVal1/holdVal2);
  return(estOrder, holdVal1, holdVal2);
end
