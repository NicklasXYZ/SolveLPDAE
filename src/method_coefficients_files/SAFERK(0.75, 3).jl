# SAFERK method: beta = 0.5, s = 3
const beta = 0.75;
const a21 = beta*(3-beta)/6;
const a22 = beta*(3-2*beta)/(6*(1-beta));
const a23 = -beta^3/(6*(1-beta));
const a31 = (-1+3*beta)/(6*beta);
const a32 = 1/(6*beta*(1-beta));
const a33 = (2-3*beta)/(6*(1-beta));
RKmat = [0. 0. 0.; a21 a22 a23; a31 a32 a33];
c = [0.; beta; 1.];
b = transpose([a31; a32; a33]);
