# Radau-IIA method: Order 5
RKmat = [(88-7*sqrt(6))/360 (296-169*sqrt(6))/1800 (-2+3*sqrt(6))/225;
         (296+169*sqrt(6))/1800 (88+7*sqrt(6))/360 (-2-3*sqrt(6))/225;
         (16-sqrt(6))/36 (16+sqrt(6))/36 1/9];
c = [(4-sqrt(6))/10; (4+sqrt(6))/10; 1.];
b = transpose([(16-sqrt(6))/36; (16+sqrt(6))/36; 1/9]);
