# SAFERK method: beta = 0.74, s = 4
beta = 0.74;
const a21 = (-3+5*beta)*(-21+200*beta-565*beta^2+500*beta^3)/(1500*beta*(-1+2*beta)^3);
const a22 = (-3+5*beta)*(-33+220*beta-505*beta^2+400*beta^3)/(60*(-1+2*beta)*(-2+5*beta)*(3-10*beta+10*beta^2));
const a23 = (-3+5*beta)^3*(-7+15*beta)/(1500*beta*(beta-1)*(-1+2*beta)^3*(3-10*beta+10*beta^2));
const a24 = (-3+5*beta)^3*(3-15*beta+20*beta^2)/(1500*(1-beta)*(-1+2*beta)^3*(-2+5*beta));
const a31 = beta*(-18+46*beta-35*beta^2+10*beta^3)/(12*(-3+5*beta));
const a32 = (125*(2-beta)*beta^3*(-1+2*beta)^3)/(12*(-3+5*beta)*(-2+5*beta)*(3-10*beta+10*beta^2));
const a33 = beta*(-18+62*beta-75*beta^2+30*beta^3)/(12*(beta-1)*(3-10*beta+10*beta^2));
const a34 = beta^3*(6-15*beta+10*beta^2)/(12*(beta-1)*(-2+5*beta));
const a41 = (1-8*beta+10*beta^2)/(12*beta*(-3+5*beta));
const a42 = (125*(-1+2*beta)^4)/(12*(-3+5*beta)*(-2+5*beta)*(3-10*beta+10*beta^2));
const a43 = 1/(12*beta*(1-beta)*(3-10*beta+10*beta^2));
const a44 = (3-12*beta+10*beta^2)/(12*(beta-1)*(-2+5*beta));
RKmat = [0 0 0 0; a21 a22 a23 a24; a31 a32 a33 a34; a41 a42 a43 a44];
c = [0; (3-5*beta)/(5*(1-2*beta)); beta; 1];
b = transpose([a41; a42; a43; a44]);
