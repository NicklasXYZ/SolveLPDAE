###### Author: Nicklas Sindlev Andersen

# Introduction

The following module is written in [Julia](http://julialang.org/), a high-performance dynamic programming language. The syntax of Julia is similar to R, python and MATLAB and the performance approaches that of the C programming language. Julia is thus a very suitable language for numerical and scientific computing.

The module SolveLPDAE's main functionality, is to numerically solve a linear partial differential algebraic equation (abbreviated PDAE) initial-boundary value problem (abbreviated IBVP) given all the needed information for discretizing the problem in space by finite differences and advancing the solution in time with a Runge-Kutta method. In addition to this, the module implements a number of other procedures, that are briefly summarized here. It is additionally possible to:
- Select between Runge-Kutta methods: Radau-IIA or SAFERK methods to numerically advance the solution in time.
- Calculate the exact solution at the space and time discretization points.
- Compute the error between the approximate and exact solution (at the space and time discretization points).
- Estimate the convergence order in the discrete $L_2$-norm.
- Plot a solution to a component of the linear PDAE given the exact or approximate solution to the PDAE IBVP.   

To make things easier, we will from now one, when talking about PDAEs, be referring to linear PDAEs defined and described by [1].

## Limitations

- In regards to boundary conditions it should be mentioned, that the module only implements (inhomogeneous and homogeneous) Dirichlet boundary conditions.

- To advance the solution of the semi-discretized PDAE, in time, with a Runge-Kutta method, a system ``Ax=b`` needs to be solved at each time step. The module implements sparse matrices and the linear system are therefore solved by [UMFPACK](http://faculty.cse.tamu.edu/davis/suitesparse.html). Unfortunately UMFPACK does not implement arbitrary precision and rounding errors will occur, when trying to solve ``Example1.jl``. In this case another implementation of this module have been used specifically for this example. The structure of the module is the same and the module below, can be altered slightly to accomodate arbitraty precision, however, at the expense of speed.

## Usage

To use the SolveLPDAE module we need to import it. This is done by calling the following command
```julia
using SolveLPDAE
```

For a demonstration of how to solve a PDAE IBVP with this module, we consider the following example without going into too much detail. For the definition of a linear PDAE and the process of solving a PDAE IBVP by applying Runge-Kutta methods, see [1]. For an in-depth explanation and statement of the following example/problem see [2].

#### Example 2-1

Let `Example2-1.jl` be a Julia file containing the various matrices ``A``, ``B``, ``C``, a parameter ``r`` and a vector of functions ``f``, that define the PDAE. The file should additionally contain some other information such as
- Discretization parameters ``timeSteps`` and ``spaceSteps``, that respectively define the number of time steps and spatial grid points. Another parameter ``delta``, should also be defined. This parameter defines whether we should discretize the differential operator $B\dfrac{\partial}{\partial}$ in the PDAE,  by first or second order differences.
- The specification of a finite domain for the independent variables i.e., we should define intervals ``[xS, xE]``, ``[tS, tE]`` for the variables $x$ and $t$.
- A vector of functions ``u``, that contains the exact solution to the PDAE.

All this information will be needed, when setting up and solving the PDAE IBVP.


From now on we will refer to a file like ``Example2-1.jl``, as a _PDAE-problem file_. To clarify what exactly is needed to be defined in such a file, the content of one is shown here below:

<p align="center">
```Example 2-1```
</p>
```julia
const r = 0.;
const delta = 0.5;
const xS = -1.;
const xE = 1.;
const tS = 0.;
const tE = 1.;
const timeSteps = 25;
const spaceSteps = 25;
A = [0. 2. 0.; 1. -1. 0.; 1. -1. 0.];
B = [-1. 0. 0.; 0. 0. 0.; 0. 0. -1.];
C = [0. 0. 0.; 0. -1. 0.; 0. 0. 1.];
u = [u1(t, x) = x^2*exp(-t);
    u2(t, x) = x^2*exp(-1/2*t);
    u3(t, x) = x^2*sin(t)];
f = [f1(t, x) = -exp(-t/2)*x^2-2*exp(-t);
    f2(t, x) = -exp(-t)*x^2+(exp(-t/2)*x^2)/2-x^2*exp(-t/2);
    f3(t, x) = -exp(-t)*x^2+(exp(-t/2)*x^2)/2-2*sin(t)+sin(t)*x^2];
```
(The above Julia file is a part of the module and can be found in the module directory under 'pdae_problems'.)

To get started solving a PDAE IBVP, we need to include a PDAE-problem file in our code such that the different components in the selected file can be defined and thus used. This is done by writing
```julia
include(getExampleFilePath("Example2-1.jl"));
```
The implemented method ``getExampleFilePath()``,  takes a string as an argument and searches the module directory and returns the file path of the searched file. In this case we want to include the ``Example2-1.jl`` file and thus we pass the string ``Example2-1.jl`` as an argument to the method. The Julia method ``include()`` will then load the file at runtime (execution).
The currently available PDAE-problem files are:
- ``Example1.jl``
- ``Example2-1.jl``
- ``Example2-2.jl``
- ``Example3.jl``

After including the ``Example2-1.jl`` PDAE-problem file, we create an object that encompasses all the information given in the file. We do this by defining
```julia
moldae = setupMOLDAE(A, B, C, r, delta, xS, xE, tS, tE, timeSteps, spaceSteps, u, f);
```
(As the method name suggests, this method essentially discretizes the PDAE in space and defines a method-of-lines differential algebraic equation (abbreviated MOL-DAE).)

In addition to the above-mentioned files, it is also possible to include other PDAE-problem files by simply specifying the file path and appropriately calling the ``include()`` method. (Note: If one wishes to solve other PDAE IBVP and thus use other PDAE-problem files, than those already in the module directory, then these files should follow the same structure as the aforementioned ``Example2-1.jl`` file.)

The next step is to choose a Runge-Kutta method. As mentioned before it is possible to choose between Radau-IIA and SAFERK methods. Now, if we wish use a SAFERK method, then we would need to specify the following
```julia
solutionMethod = SolveLPDAE.SAFERK
```
If we instead wanted to use a Radau-IIA method, then we would write
```julia
solutionMethod = SolveLPDAE.RadauIIA
```
Let us continue the example with a SAFERK method. In this case we would also need to specify a _Runge-Kutta matrix_ ``RKmat``, arrays ``b`` and ``c`` that respectively hold the _weights_ and _nodes_. These quantities define a particular SAFERK method and are specified in a Julia file, which we from now on, will call a _method coefficient file_. We now include a method coefficient file for a 3-stage SAFERK method. This is done as follows
```julia
include(getMethodFilePath("SAFERK(0.75, 3)"));
```
The implemented method ``getMethodFilePath()`` works the same way, as the ``getExampleFilePath()`` method mentioned earlier. The only difference in this case is, that the method searches for method coefficient files and not PDAE-problem files.
The currently available SAFERK method coefficient files are:
- ``SAFERK(0.75, 3).jl``     
- ``SAFERK(0.73, 4).jl``
- ``SAFERK(0.74, 4).jl``
- ``SAFERK(0.75, 4).jl``

For the Radau-IIA method, available method coefficient files are:  
- ``RadauIIA(1).jl``
- ``RadauIIA(3).jl``
- ``RadauIIA(5).jl``

Like before, we continue on and create an object that encompasses all the information given in the method coefficient file. This is done by defining
```julia
method = setupRungeKuttaMethod(solutionMethod, RKmat, b, c);
```
We are now ready to numerically solve the PDAE IBVP by advancing in time, the resulting (from the spatial discretization of the PDAE) MOL-DAE. We do this by passing the two objects ``moldae`` and ``method`` to the following method
```julia
approxU = solveMOLDAE(moldae, method);
```
The variable ``approxU`` now holds the approximate solution to the PDAE IVBP. To plot a solution to a particular component of the PDAE, we call the two methods
```julia
solIndex = 1;
approxSol = getSolution(moldae, approxU, solIndex);
plotSolution(moldae, approxSol, solIndex);
```
Here ``solIndex`` indicates which component we are interested in obtaining and plotting the solution to. In this case the approximate solution to the first component has been picked.

If we are now interested in obtaining an estimate of the order of convergence for the 3-stage SAFERK method on the PDAE defined in the ``Example2-1.jl`` PDAE-problem file, then we first need to define
```julia
timeSteps1 = 10;
timeSteps2 = 20;
spaceSteps1 = 39;
spaceSteps2 = 39;
moldae1 = setupMOLDAE(A, B, C, r, delta, xS, xE, tS, tE, timeSteps1, spaceSteps1, u, f);
moldae2 = setupMOLDAE(A, B, C, r, delta, xS, xE, tS, tE, timeSteps2, spaceSteps2, u, f);
```
The two objects ``moldae1`` and ``moldae2`` defines the same problem, just with two different time step sizes. The idea now, is to find the approximate and exact solution, along with the error between these, for each of the two objects. We write
```julia
approxU1 = solveMOLDAE(moldae1, method);
approxU2 = solveMOLDAE(moldae2, method);
exactU1 = calcExactSolution(moldae1);
exactU2 = calcExactSolution(moldae2);
```
Going from one mesh size to a more refined mesh, we are able to calculate the error (at the point $x = x_{end}$, $t = t_{end}$) for the two respective meshes and then use the two obtained error values, to get an estimate of the order of convergence in the $L_2$-norm. A single estimate of the order of convergence at the mesh point $x = x_{end}$, $t = t_{end}$ is given by calling the method
```julia
estConvergenceOrder(moldae1, moldae2, approxU1, approxU2, exactU1, exactU2)
```   

For the sake of completeness we conclude the example by giving the full code here below:

###### Numerical solution to a PDAE IBVP
```julia
using SolveLPDAE

### Include PDAE-problem file
include(getExampleFilePath("Example2-1.jl"));

### Setup MOL-DAE object
moldae = setupMOLDAE(A, B, C, r, delta, xS, xE, tS, tE, timeSteps, spaceSteps, u, f);

### Select Runge-Kutta method
solutionMethod = SolveLPDAE.SAFERK

### Include method coefficient file for a particular SAFERK method
include(getMethodFilePath("SAFERK(0.75, 3)"));

### Setup Runge-Kutta method object
method = setupRungeKuttaMethod(solutionMethod, RKmat, b, c);

### Get the approximate solution to the PDAE IBVP
approxU = solveMOLDAE(moldae, method)

### Pick a solution to a particular component of the PDAE
solIndex = 1;
approxSol = getSolution(moldae, approxU, solIndex)

### Plot the solution to the chosen component
plotSolution(moldae, approxSol, solIndex);
```

###### Convergence estimate in the L2-norm
```julia
using SolveLPDAE

### Select the number of spatial grid points and time steps
timeSteps1 = 10;
timeSteps2 = 20;
spaceSteps1 = 39;
spaceSteps2 = 39;

### Define two objects of the same problem, but one with double the number of time steps
moldae1 = setupMOLDAE(A, B, C, r, delta, xS, xE, tS, tE, timeSteps1, spaceSteps1, u, f);
moldae2 = setupMOLDAE(A, B, C, r, delta, xS, xE, tS, tE, timeSteps2, spaceSteps2, u, f);

### Get the approximate solution to the PDAE IBVP
approxU1 = solveMOLDAE(moldae1, method);
approxU2 = solveMOLDAE(moldae2, method);

### Get the exact solution to the PDAE at the space and time discretization points
exactU1 = calcExactSolution(moldae1);
exactU2 = calcExactSolution(moldae2);

### Calculate the error between the approximate and exact solutions and return an estimate
### of the order of convergence in the discrete L2-norm.
estConvergenceOrder(moldae1, moldae2, approxU1, approxU2, exactU1, exactU2)
```

## References

[1] Nicklas Sindlev Andersen, Implementation and Analysis of New Time Integration Methods for the Solution of Partial Differential Algebraic Equations.

[2] ---II---, Section 4.
